CC = gcc
CC_FLAGS = -Wall

CXX = g++
CXX_FLAGS = -Wall

OBJECTS = main.o executioner.o tree.o list.o

rel_%.o: %.c
	$(CC) -c -O2 -DNDEBUG $(CFLAGS) $< -o $@

dbg_%.o: %.c
	$(CC) -c -g $(CFLAGS) $< -o $@

release: solution
debug: solution.dbg

solution: $(addprefix rel_, $(OBJECTS))
	$(CC) -o solution -O2 $^

solution.dbg: $(addprefix dbg_, $(OBJECTS))
	$(CC) -g -o solution.dbg $^

test.o: test.cpp
	$(CXX) -c -g $(CXX_FLAGS) test.cpp -o test

test: test.o solution.dbg
	$(CXX) $(CXX_FLAGS) -lgtest -lgtest_main test.cpp dbg_executioner.o dbg_tree.o dbg_list.o -o test

zip:
	zip ATree.zip *.c *.h test.cpp Doxyfile Makefile CommandLoadingTest.txt test.sh

documentation: Doxyfile $(wildcard *.h)
	doxygen

clean:
	@rm -f *.o
	@rm -f solution
	@rm -f solution.dbg
	@rm -f test
	@rm -rf doc/
	@rm -f doxygen.log
	@rm -f ATree.zip

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "executioner.h"

static void ExecuteSingleCommand(uint8_t verbosity_level, CommandEntry command, Forest *forest);

Executioner *CreateExecutioner(uint8_t verbosity_level)
{
    Executioner *executor = (Executioner*)malloc(sizeof(Executioner));
    executor->verbosityLevel = verbosity_level;
    executor->first = executor->last = (CommandBlock*)malloc(sizeof(CommandBlock));
    executor->first->next = NULL;
    executor->topLastIndex = 0;
    executor->requiredSize = 1;
    return executor;
}

void PushCommand(Executioner *executioner, CommandT command, NodeIdT arg_k, NodeIdT arg_w)
{
    assert(executioner != NULL);

    if (executioner->topLastIndex >= COMMAND_BLOCK_SIZE) {
        //Alloc new block
        CommandBlock *new_block = (CommandBlock*)malloc(sizeof(CommandBlock));
        new_block->next = NULL;
        executioner->last->next = new_block;
        executioner->last = new_block;
        executioner->topLastIndex = 0;
    }

    CommandEntry *new_command = executioner->last->commands + executioner->topLastIndex;
    ++executioner->topLastIndex;
    new_command->command = command;
    new_command->argK = arg_k;
    new_command->argW = arg_w;
}

void ExecuteCommands(Executioner *executioner, Forest *forest)
{
    assert(executioner != NULL);
    assert(forest != NULL);

    for (CommandBlock *block = executioner->first; block != NULL; block = block->next) {
        const size_t block_bound = block->next == NULL ? executioner->topLastIndex : COMMAND_BLOCK_SIZE;
        for (size_t command_i = 0; command_i < block_bound; ++command_i)
            ExecuteSingleCommand(executioner->verbosityLevel, block->commands[command_i], forest);
    }
}

void LoadCommands(Executioner *target_executioner, FILE *file)
{
    assert(target_executioner != NULL);
    assert(file != NULL);

    const size_t BUF_SIZE = 32;
    char read_str[BUF_SIZE];
    NodeIdT w, k;

    while (!feof(file)) {
        if (fscanf(file, "%31s", read_str) < 1)
            continue;

        if (strncmp(read_str, "ADD_NODE", BUF_SIZE) == 0) {
            fscanf(file, "%u", &k);
            if (target_executioner->verbosityLevel > 1 && k >= target_executioner->requiredSize)
                fprintf(stderr, "Too big node id\n");
            ++target_executioner->requiredSize;
            PushCommand(target_executioner, COMMAND_ADD_NODE, k, 0);

        } else if (strncmp(read_str, "RIGHTMOST_CHILD", BUF_SIZE) == 0) {
            fscanf(file, "%u", &k);
            if (target_executioner->verbosityLevel > 1 && k >= target_executioner->requiredSize)
                fprintf(stderr, "Too big node id\n");
            PushCommand(target_executioner, COMMAND_RIGHTMOST_CHILD, k, 0);

        } else if (strncmp(read_str, "DELETE_NODE", BUF_SIZE) == 0) {
            fscanf(file, "%u", &k);
            if (target_executioner->verbosityLevel > 1 && k >= target_executioner->requiredSize)
                fprintf(stderr, "Too big node id\n");
            PushCommand(target_executioner, COMMAND_DELETE_NODE, k, 0);

        } else if (strncmp(read_str, "DELETE_SUBTREE", BUF_SIZE) == 0) {
            fscanf(file, "%u", &k);
            if (target_executioner->verbosityLevel > 1 && k >= target_executioner->requiredSize)
                fprintf(stderr, "Too big node id\n");
            PushCommand(target_executioner, COMMAND_DELETE_SUBTREE, k, 0);

        } else if (strncmp(read_str, "SPLIT_NODE", BUF_SIZE) == 0) {
            fscanf(file, "%u %u", &k, &w);
            if (target_executioner->verbosityLevel > 1
                && (k >= target_executioner->requiredSize || w >= target_executioner->requiredSize))
                fprintf(stderr, "Too big node id\n");
            ++target_executioner->requiredSize;
            PushCommand(target_executioner, COMMAND_SPLIT_NODE, k, w);

        } else if (read_str[0] != '\0' && target_executioner->verbosityLevel > 1) {
            fprintf(stderr, "Invalid command: %s", read_str);
            break;
        }
    }
}
void DeleteExecutioner(Executioner *executioner)
{
    if (executioner == NULL)
        return;

    for (CommandBlock *block = executioner->first; block != NULL;) {
        CommandBlock *next = block->next;
        free(block);
        block = next;
    }

    free(executioner);
}

static void ExecuteSingleCommand(uint8_t verbosity_level, CommandEntry command, Forest *forest)
{
    NodeIdT returned_node;
    switch (command.command) {
        case COMMAND_ADD_NODE:
            AddNode(forest, command.argK);
            break;
        case COMMAND_RIGHTMOST_CHILD:
            returned_node = RightmostChild(forest, command.argK);
            printf("%i\n", returned_node == 0 ? -1 : (int)returned_node);
            break;
        case COMMAND_DELETE_NODE:
            DeleteNode(forest, command.argK);
            break;
        case COMMAND_DELETE_SUBTREE:
            DeleteSubtree(forest, command.argK);
            break;
        case COMMAND_SPLIT_NODE:
            SplitNode(forest, command.argK, command.argW);
            break;
        default:
            if (verbosity_level > 1)
                fprintf(stderr, "Invalid command\n");
            break;
    }

    if (verbosity_level > 0)
        fprintf(stderr, "NODES: %u\n", forest->size);
    if (command.command != COMMAND_RIGHTMOST_CHILD)
        printf("OK\n");
}

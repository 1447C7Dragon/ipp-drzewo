/** @file
 * Data structure used for buffering/executing commands.
 */
#ifndef ATREE_COMMANDQUEUE_H
#define ATREE_COMMANDQUEUE_H

#include <inttypes.h>
#include "tree.h"

///Type for COMMAND_ enum (let's say it is enum)
typedef uint8_t CommandT;

///Command ADD_NODE <k>
#define COMMAND_ADD_NODE 0

///Command RIGHTMOST_CHILD <k>
#define COMMAND_RIGHTMOST_CHILD 1

///Command DELETE_NODE <k>
#define COMMAND_DELETE_NODE 2

///Command DELETE_SUBTREE <k>
#define COMMAND_DELETE_SUBTREE 3

///Command SPLIT_NODE <k> <w>
#define COMMAND_SPLIT_NODE 4

///Number of command in single <c>CommandBlock</c>
#define COMMAND_BLOCK_SIZE 127

/**
 * Representation of single command
 */
typedef struct CommandEntry
{
    ///Command parameter described as <k>
    NodeIdT argK;

    ///Command parameter described as <w>. Ignored if <c>command != COMMAND_SPLIT_NODE</c>
    NodeIdT argW;

    ///One byte command code as in <c>COMMAND_*</c> enum
    CommandT command;
} CommandEntry;

/**
 * Hunk of commands array used by executioner
 */
typedef struct CommandBlock
{
    ///Commands stored in block
    CommandEntry commands[COMMAND_BLOCK_SIZE];

    ///Pointer to nex hunk
    struct CommandBlock *next;
} CommandBlock;

/**
 * Structure that loads, holds and executes desired operations on tree.
 */
typedef struct Executioner
{
    ///1 for -v flag; 2 for debugging; 0 in other case
    uint8_t verbosityLevel;

    ///Index of first free position in last hunk (if <c>>= COMMAND_BLOCK_SIZE</c> it means that there is no free space)
    size_t topLastIndex;

    ///Required size of tree
    NodeIdT requiredSize;

    ///First hunk of commands list
    CommandBlock *first;

    ///Last hunk of commands list
    CommandBlock *last;
} Executioner;

/**
 * Initialise executioner and set desired verbosity level.
 * @param verbosity_level verbosity level to be set in executioner
 * @return pointer to newly alocated executioner
 */
Executioner *CreateExecutioner(uint8_t verbosity_level);

/**
 * Free memory used by executioner.
 * @param executioner pointer to executioner
 */
void DeleteExecutioner(Executioner *executioner);

/**
 * Append command to executioner's buffer (after all commends ther were already pushed).
 * @param executioner executioner to which command will be added
 * @param command command code as in <c>COMMAND_*</c> enum
 * @param arg_k k parameter for command
 * @param arg_w w parameter for command (unused unless <c>command = COMMAND_SPLIT_NODE</c>
 */
void PushCommand(Executioner *executioner, CommandT command, NodeIdT arg_k, NodeIdT arg_w);

/**
 * Execute all commands on list on given tree.
 * @param executioner list of commands
 * @param forest tree on which commands will be executed
 */
void ExecuteCommands(Executioner *executioner, Forest *forest);

/**
 * Load to command list commands from file. Commands are loaded from given file and it should be correctly opened and
 * closed after this function ends.
 * @param target_executioner target commands list
 * @param file file with commands
 */
void LoadCommands(Executioner *target_executioner, FILE *file);

#endif //ATREE_COMMANDQUEUE_H

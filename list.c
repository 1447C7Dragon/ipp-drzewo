#include <assert.h>
#include <stdlib.h>

#include "list.h"


List *ListCreate(void) {
    List *list = (List*)malloc(sizeof(List));
    list->head = NULL;
    list->tail = NULL;
    return list;
}

ListElement *ListInsertAfter(List *list, ListElement *element, uint32_t new_element_data) {
    ListElement *new_element = (ListElement*)malloc(sizeof(ListElement));
    new_element->data = new_element_data;

    if (list != NULL && list->head == NULL) {
        new_element->next = NULL;
        new_element->prev = NULL;
        list->head = new_element;
        list->tail = new_element;

    } else if (list != NULL && element == NULL) {
        new_element->prev = NULL;
        new_element->next = list->head;
        list->head->prev = new_element;
        list->head = new_element;

    } else {
        assert(element != NULL);
        ListElement *next_element = element->next;
        new_element->prev = element;
        element->next = new_element;
        new_element->next = next_element;

        if (list != NULL && element == list->tail) {
            list->tail = new_element;
        } else {
            assert(next_element != NULL);
            next_element->prev = new_element;
        }
    }

    return new_element;
}

void ListDelete(List *list) {
    ListClear(list);
    free(list);
}

void ListDevourAfter(List *list, ListElement *element, List *elements) {
    if (elements->head == NULL)
        return;

    if (list != NULL && list->head == NULL) {
        list->head = elements->head;
        list->tail = elements->tail;

    } else if (list != NULL && element == NULL) {
        elements->tail->next = list->head;
        list->head->prev = elements->tail;
        list->head = elements->head;

    } else {
        assert(element != NULL);
        ListElement *next_element = element->next;
        elements->head->prev = element;
        element->next = elements->head;
        elements->tail->next = next_element;

        if (list != NULL && element == list->tail) {
            list->tail = elements->tail;
        } else {
            assert(next_element != NULL);
            next_element->prev = elements->tail;
        }
    }

    elements->head = NULL;
    elements->tail = NULL;
}

void ListErase(List *list, ListElement *element)
{
    assert(element != NULL);
    ListElement *prev = element->prev;
    ListElement *next = element->next;

    if (prev != NULL) {
        prev->next = next;
    } else {
        assert(list != NULL);
        list->head = next;
    }

    if (next != NULL) {
        next->prev = prev;
    } else {
        assert(list != NULL);
        list->tail = prev;
    }

    free(element);
}

List *ListSplitAfter(List *list, ListElement *pivot) {
    List *new_list = ListCreate();

    if (pivot == list->tail) {
        //Nothing to be done

    } else if (pivot == NULL) {
        new_list->head = list->head;
        new_list->tail = list->tail;
        list->head = NULL;
        list->tail = NULL;

    } else {
        new_list->head = pivot->next;
        new_list->tail = list->tail;
        list->tail = pivot;

        pivot->next = NULL;
        new_list->head->prev = NULL;
    }

    return new_list;
}

void ListClear(List *list)
{
    for (ListElement *elem = list->head; elem != NULL;) {
        ListElement *next = elem->next;
        free(elem);
        elem = next;
    }
    list->tail = NULL;
    list->head = NULL;
}

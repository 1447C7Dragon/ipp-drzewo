/** @file
 * Implementation of the bidirectional list structure
 */
#ifndef ATREE_LIST_H
#define ATREE_LIST_H

#include <stddef.h>
#include <inttypes.h>

/**
 * Single element of bidirectional list.
 */
typedef struct ListElement
{
    ///Pointer to previous element in list
    struct ListElement *prev;

    ///Pointer to nest element in list
    struct ListElement *next;

    ///Data assigned with this element
    uint32_t data;
} ListElement;

/**
 * Bidirectional list.
 */
typedef struct List
{
    ///First element of list
    ListElement *head;

    ///Last element of list
    ListElement *tail;
} List;

/**
 * Create new empty bidirectional list. Constant time.
 * @return New empty bidirectional list
 */
List *ListCreate(void);

/**
 * Insert new element after given element. Create new element with <c>data = new_element_data</c> and insert it into
 * list after <c>element</c>. If <c>last_element == NULL</c> then list is prepended. Time is constant.
 * @param list bidirectional list containing <c>element</c>
 * @param element element of list, after which new element should be placed (not null)
 * @param new_element_data data for new element
 * @return pointer to new element
 */
ListElement *ListInsertAfter(List *list, ListElement *element, uint32_t new_element_data);

/**
 * Merge two lists: append all elements of <c>list</c> after <c>element</c>. After that operation <c>elements</c>
 * becomes an empty list (it is devoured). Order of inserted elements is preserved. When
 * <c>element == NULL</c> then elements are appended at the beginning of list. Call to this function is still valid wih
 * <c>list = NULL</c> if <c>element</c> is not tail nor <c>NULL</c>. Time is constant
 * @param list bidirectional list containing <c>element</c> (can be <c>NULL</c> if element is not tail nor <c>NULL</c>)
 * @param element element after which content of <c>list</c> should be placed
 * @param list elements to be placed
 */
void ListDevourAfter(List *list, ListElement *element, List *elements);

/**
 * Split list in two lists. Operation is valid even if one of lists (result or the modified one) will be empty and it
 * works as expected. Time is constant.
 * @param list
 * @param pivot
 * @return
 */
List *ListSplitAfter(List *list, ListElement *pivot);

/**
 * Remove element from list. After removing element it is still possible to use elements pointed by <c>next</c> and
 * <c>prev</c>. List can be <c>NULL</c> unless element is <c>NULL</c> or tail. Time is constant.
 * @param list bidirectional list containing <c>element</c>
 * @param element element to be removed
 */
void ListErase(List *list, ListElement *element);

/**
 * Free memory used by bidirectional list and optionally by data held in it. Memory used by data stored in list should
 * be deleted if and only if <c>remove_data != 0</c>. Time is linear by number of elements in list.
 * @param list list to be freed
 * @param remove_data boolean indicating whether memory used by data in list should be freed
 */
void ListDelete(List *list);

/**
 * Remove all elements from list, but leave the list itself. Set list's head and tail to <c>NULL</c>. Time is linear by
 * number of elements in list.
 * @param list list to be cleared
 */
void ListClear(List *list);

#endif //ATREE_LIST_H

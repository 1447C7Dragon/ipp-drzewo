#include <string.h>
#include <stdio.h>

#include "tree.h"
#include "executioner.h"

///Return code, when no errors encountered
const int EXITCODE_OK = 0;
///Program's return code, when invalid parameters were passed
const int EXITCODE_INVALID_INVOCATION = 1;




int main(int argc, char **argv)
{
    uint8_t verbosity = 0;
    if (argc > 1) {
        if (argc == 2 && strcmp(argv[1], "-v") == 0) {
            verbosity = 1;
        } else {
            printf("ERROR\n");
            return EXITCODE_INVALID_INVOCATION;
        }
    }

    Executioner *main_executioner = CreateExecutioner(verbosity);
    LoadCommands(main_executioner, stdin);
    Forest *pale_tree = CreateForest(main_executioner->requiredSize);
    ExecuteCommands(main_executioner, pale_tree);
    DeleteForest(pale_tree);
    DeleteExecutioner(main_executioner);

    return EXITCODE_OK;
}

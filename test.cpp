#include <gtest/gtest.h>
#include <list>
#include <random>
#include <stack>
#include <functional>

extern "C" {
#include "list.h"
#include "executioner.h"
}


//************************************
// * ** ***    LIST TESTS     *** ** *
//************************************
List *FromStlList(std::list<uint32_t > v)
{
    List *list = ListCreate();
    for (uint32_t i : v) {
        ListInsertAfter(list, list->tail, i);
    }
    return list;
}

bool EqualsStlList(List *list, const std::list<uint32_t> v)
{
    auto it = v.cbegin();
    for (ListElement *e = list->head; e != nullptr; e = e->next, ++it) {
        if (e->data != *it)
            return false;
    }
    return true;
}

TEST(List, InsertAfter)
{
    std::list<uint32_t > items{1, 2, 3, 4, 5};
    //This appends
    List *list = FromStlList(items);
    ASSERT_TRUE(EqualsStlList(list, items));

    //This prepends
    items.push_front(7);
    ListElement *insert_result = ListInsertAfter(list, nullptr, 7);
    ASSERT_EQ(insert_result->data, 7u);
    items.push_front(3);
    ListInsertAfter(list, nullptr, 3);
    ASSERT_EQ(insert_result->data, 7u);
    items.push_front(1);
    insert_result = ListInsertAfter(list, nullptr, 1);
    ASSERT_EQ(insert_result->data, 1u);
    ASSERT_TRUE(EqualsStlList(list, items));

    //This puts in middle
    auto it = items.begin();
    ++it;
    items.insert(it, 0);
    ListInsertAfter(nullptr, list->head, 0);
    ASSERT_TRUE(EqualsStlList(list, items));

    ListDelete(list);
}

TEST(List, ExtendAfter)
{
    std::list<uint32_t > inserted{2, 3, 5};
    std::list<uint32_t > items{1, 1, 1, 1, 1, 1};
    List *list = FromStlList(items);
    ASSERT_TRUE(EqualsStlList(list, items));

    //Prepend
    List *elements = FromStlList(inserted);
    ListDevourAfter(list, nullptr, elements);
    ListDelete(elements);
    items.insert(items.begin(), inserted.begin(), inserted.end());
    ASSERT_TRUE(EqualsStlList(list, items));

    //Append
    elements = FromStlList(inserted);
    ListDevourAfter(list, list->tail, elements);
    ListDelete(elements);
    items.insert(items.end(), inserted.begin(), inserted.end());
    ASSERT_TRUE(EqualsStlList(list, items));

    //After first
    elements = FromStlList(inserted);
    ListDevourAfter(nullptr, list->head, elements); //Not at end so nullptr should be valid
    ListDelete(elements);
    auto it = items.begin();
    ++it;
    items.insert(it, inserted.begin(), inserted.end());
    ASSERT_TRUE(EqualsStlList(list, items));

    ListDelete(list);
}

TEST(List, Erase)
{
    std::list<uint32_t > numbers{8, 11, 324, 2, 4, 2, 17, 33, 34, 232, 11, 12, 16};
    List *list = FromStlList(numbers);

    //Remove even
    for (ListElement *e = list->head; e != nullptr;) {
        ListElement *next = e->next;
        if (e->data % 2 == 0)
            ListErase(list, e);
        e = next;
    }
    std::list<uint32_t > expected_out{11, 17, 33, 11};
    ASSERT_TRUE(EqualsStlList(list, expected_out));

    //Remove all
    for (ListElement *e = list->head; e != nullptr;) {
        ListElement *next = e->next;
        ListErase(list, e);
        e = next;
    }
    ASSERT_EQ(list->head, nullptr);
    ASSERT_EQ(list->tail, nullptr);

    ListDelete(list);
}

TEST(List, BigAndRandom)
{
    const int SIZE = 10000000;
    const int REMOVAL_DIVISOR = 3;

    List *list = ListCreate();
    for (int i = 0; i < SIZE; ++i)
        ListInsertAfter(list, list->head, rand() % 100u);

    for (ListElement *e = list->head; e != nullptr;) {
        ListElement *next = e->next;
        if (e->data % REMOVAL_DIVISOR == 0)
            ListErase(list, e);
        e = next;
    }

    ListDelete(list);
}

TEST(List, Spliting)
{
    std::list<uint32_t > elements{1, 2, 3, 5, 6, 7};
    std::list<uint32_t > elem{1, 2, 3};
    std::list<uint32_t > ents{5, 6, 7};
    List *list = FromStlList(elements);
    ListElement *head = list->head;
    ListElement *tail = list->tail;

    List *null_suf = ListSplitAfter(list, list->tail);
    ASSERT_EQ(null_suf->head, nullptr);
    ASSERT_EQ(null_suf->tail, nullptr);
    ASSERT_EQ(list->head, head);
    ASSERT_EQ(list->tail, tail);

    List *moved = ListSplitAfter(list, nullptr);
    ASSERT_EQ(list->head, nullptr);
    ASSERT_EQ(list->tail, nullptr);
    ASSERT_EQ(moved->head, head);
    ASSERT_EQ(moved->tail, tail);

    ListElement *pivot = moved->head->next->next;
    ListElement *next_to_pivot = pivot->next;
    List *suf = ListSplitAfter(moved, pivot);
    ASSERT_EQ(moved->head, head);
    ASSERT_EQ(moved->tail, pivot);
    ASSERT_EQ(pivot->next, nullptr);
    ASSERT_EQ(next_to_pivot->prev, nullptr);
    ASSERT_EQ(suf->head, next_to_pivot);
    ASSERT_EQ(suf->tail, tail);

    ListDelete(list);
    ListDelete(null_suf);
    ListDelete(moved);
    ListDelete(suf);
}


//************************************
// * ** *** EXECUTIONER TESTS *** ** *
//************************************

TEST(Executioner, BuildingExecutioner)
{
    Executioner *champion_bandit_executioner = CreateExecutioner(3);

    for (unsigned int i = 0; i < 4000; ++i) {
        switch (i % 3) {
            case 0:
                PushCommand(champion_bandit_executioner, COMMAND_ADD_NODE, i % 3, 0);
                break;
            case 1:
                PushCommand(champion_bandit_executioner, COMMAND_RIGHTMOST_CHILD, i % 3, 0);
                break;
            case 2:
                PushCommand(champion_bandit_executioner, COMMAND_SPLIT_NODE, i, i);
                break;
            default:
                break;
        }
    }

    unsigned int i = 0;
    for (CommandBlock *block = champion_bandit_executioner->first; block != NULL; block = block->next) {
        const auto iter_max = block->next == NULL ? champion_bandit_executioner->topLastIndex : COMMAND_BLOCK_SIZE;
        for (unsigned int block_iter = 0; block_iter < iter_max; ++block_iter) {
            switch (i % 3) {
                case 0:
                    ASSERT_EQ(block->commands[block_iter].command, COMMAND_ADD_NODE) << "i = " << i;
                    ASSERT_EQ(block->commands[block_iter].argK, i % 3) << "i = " << i;
                    break;
                case 1:
                    ASSERT_EQ(block->commands[block_iter].command, COMMAND_RIGHTMOST_CHILD) << "i = " << i;
                    ASSERT_EQ(block->commands[block_iter].argK, i % 3) << "i = " << i;
                    break;
                case 2:
                    ASSERT_EQ(block->commands[block_iter].command, COMMAND_SPLIT_NODE) << "i = " << i;
                    ASSERT_EQ(block->commands[block_iter].argK, i) << "i = " << i;
                    ASSERT_EQ(block->commands[block_iter].argW, i) << "i = " << i;
                    break;
                default:
                    break;
            }
            ++i;
        }
    }

    DeleteExecutioner(champion_bandit_executioner);
}

TEST(Executioner, LoadingCommands)
{
    Executioner *executioner = CreateExecutioner(3);
    FILE *input = fopen("CommandLoadingTest.txt", "r");
    if (input == NULL)
        perror("File not opened");

    LoadCommands(executioner, input);
    fclose(input);

    ASSERT_EQ(executioner->first->commands[0].command, COMMAND_ADD_NODE);
    ASSERT_EQ(executioner->first->commands[1].argK, 0);
    ASSERT_EQ(executioner->first->commands[2].command, COMMAND_ADD_NODE);
    ASSERT_EQ(executioner->first->commands[3].argK, 0);

    ASSERT_EQ(executioner->first->commands[5].command, COMMAND_SPLIT_NODE);
    ASSERT_EQ(executioner->first->commands[5].argW, 3);
    ASSERT_EQ(executioner->first->commands[6].command, COMMAND_DELETE_NODE);
    ASSERT_EQ(executioner->first->commands[6].argK, 1);
    ASSERT_EQ(executioner->first->commands[7].command, COMMAND_DELETE_SUBTREE);
    ASSERT_EQ(executioner->first->commands[7].argK, 2);

    ASSERT_EQ(executioner->first->commands[9].command, COMMAND_ADD_NODE);
    ASSERT_EQ(executioner->first->commands[10].argK, 3);

    ASSERT_EQ(executioner->first->commands[13].command, COMMAND_RIGHTMOST_CHILD);
    ASSERT_EQ(executioner->first->commands[13].argK, 6);

    DeleteExecutioner(executioner);
}


//************************************
// * ** ***    TREE TESTS     *** ** *
//************************************

class SimpleNode
{
    friend class SimpleTree;
    std::list<uint32_t> _childrenIds;
    uint32_t _id;
    uint32_t _parentId;
    std::list<uint32_t>::iterator _parentSegment;
    bool _isRoot = false;
public:
    explicit SimpleNode(uint32_t id, uint32_t parent):
            _id(id),
            _parentId(parent),
            _isRoot(true)
    {}

    explicit SimpleNode(uint32_t id, uint32_t parent, std::list<uint32_t>::iterator parent_segment):
            _id(id),
            _parentId(parent),
            _parentSegment(parent_segment)
    {}

    int32_t id() const {
        return _id;
    }

    uint32_t rightmostChild() const {
        if (_childrenIds.empty())
            return 0;
        return _childrenIds.back();
    }

    void addChild(uint32_t id) {
        _childrenIds.push_back(id);
    }

    size_t countChildren() const {
        return _childrenIds.size();
    }
};

std::ostream &operator<< (std::ostream &stream, const SimpleNode &node)
{
    return (stream << "Node(id=" << node.id() << ";  " << node.countChildren() << " children)");
}

class SimpleTree
{
    std::vector<SimpleNode *> _nodes;
    size_t _nodesCounter = 1;

    bool compareChildren(uint32_t id, std::list<uint32_t> expected, List *got, Forest *forest)
    {
        auto std_it = expected.begin();
        auto my_it = got->head;

        while (my_it != nullptr && std_it != expected.end()) {
            if (my_it->data == 0 || forest->forest[my_it->data] == nullptr) {
                my_it = my_it->next;
                continue;
            }
            if (*std_it == 0 || _nodes[*std_it] == nullptr) {
                ++std_it;
                continue;
            }
            if (my_it->data != *std_it) {
                std::cerr << "[SimpleTree::compareChildren] Node: (" << id << ") got id=" << my_it->data
                          << " expected id=" << *std_it << std::endl;
                return false;
            }
            my_it = my_it->next;
            ++std_it;
        }

        return true;
    }

public:
    SimpleTree()
    {
        _nodes.push_back(new SimpleNode(0, 0));
    }
    ~SimpleTree()
    {
        for (auto node :_nodes)
            delete node;
    }

    size_t countNodes() const {
        return _nodesCounter;
    }

    uint32_t addNode(uint32_t parent_id)
    {
        uint32_t id = (uint32_t)_nodes.size();
        auto p = _nodes[parent_id];
        p->addChild(id);
        auto seg =  p->_childrenIds.end();
        --seg;
        _nodes.push_back(new SimpleNode(id, parent_id, seg));
        ++_nodesCounter;
        return id;
    }

    void deleteNode(uint32_t id)
    {
        auto node = _nodes[id];
        if (node == nullptr)
            return;
        auto parent = _nodes[node->id()];
        parent->_childrenIds.splice(node->_parentSegment, node->_childrenIds);
        delete node;
        _nodes[id] = nullptr;
        --_nodesCounter;
    }

    void deleteSubtree(uint32_t id)
    {
        auto node = _nodes[id];
        if (node == nullptr)
            return;
        for (auto child_id : node->_childrenIds)
            deleteSubtree(child_id);
        delete node;
        _nodes[id] = nullptr;
        --_nodesCounter;
    }

    bool equals(Forest *a_tree) {
        if (a_tree->size != countNodes()) {
            std::cerr << "[SimpleTree::equals] bad nodes number: " << countNodes() << " expected, got "
                      << a_tree->size << std::endl;
            return false;
        }
        for (uint32_t id = 0; id < _nodes.size(); ++id) {
            if (_nodes[id] == nullptr && a_tree->forest[id] == nullptr) {
                //nothing
            } else if (_nodes[id] == nullptr || a_tree->forest[id] == nullptr) {
                std::cerr << "[SimpleTree::equals] invalid node: " << id << " expected " << _nodes[id]
                          << " got " << a_tree->forest[id] << std::endl;
                return false;
            } else {
                if (!compareChildren(id, _nodes[id]->_childrenIds, &a_tree->forest[id]->children, a_tree))
                    return false;
            }
        }
        return true;
    }

    const SimpleNode &node(uint32_t i) const {
        return *_nodes[i];
    }

    bool hasNode(uint32_t i) const {
        if (i >= _nodes.size())
            return false;
        return _nodes[i] != nullptr;
    }
};

TEST(Tree, Construct) {
    Forest *yggdrasil = CreateForest(16);
    SimpleTree conjured_yggdrasil;
    ASSERT_TRUE(conjured_yggdrasil.equals(yggdrasil));

    conjured_yggdrasil.addNode(0); // 1
    AddNode(yggdrasil, 0);
    ASSERT_TRUE(conjured_yggdrasil.equals(yggdrasil));
    conjured_yggdrasil.addNode(0); // 2
    AddNode(yggdrasil, 0);
    ASSERT_TRUE(conjured_yggdrasil.equals(yggdrasil));
    conjured_yggdrasil.addNode(0); // 3
    AddNode(yggdrasil, 0);
    conjured_yggdrasil.addNode(0); // 4
    AddNode(yggdrasil, 0);
    conjured_yggdrasil.addNode(0); // 5
    AddNode(yggdrasil, 0);
    ASSERT_TRUE(conjured_yggdrasil.equals(yggdrasil));

    conjured_yggdrasil.addNode(2); // 6
    AddNode(yggdrasil, 2);
    conjured_yggdrasil.addNode(3); // 7
    AddNode(yggdrasil, 3);
    conjured_yggdrasil.addNode(4); // 8
    AddNode(yggdrasil, 4);
    ASSERT_TRUE(conjured_yggdrasil.equals(yggdrasil));

    conjured_yggdrasil.addNode(7); // 9
    AddNode(yggdrasil, 7);
    conjured_yggdrasil.addNode(7); // 10
    AddNode(yggdrasil, 7);
    conjured_yggdrasil.addNode(7); // 11
    AddNode(yggdrasil, 7);
    ASSERT_TRUE(conjured_yggdrasil.equals(yggdrasil));

    DeleteForest(yggdrasil);
}

TEST(Tree, Cuting)
{
    Forest *sapling = CreateForest(32);
    SimpleTree conjured_sapling;

    AddNode(sapling, 0); //1
    conjured_sapling.addNode(0);
    AddNode(sapling, 0); //2
    conjured_sapling.addNode(0);
    AddNode(sapling, 0); //3
    conjured_sapling.addNode(0);

    AddNode(sapling, 1); //4
    conjured_sapling.addNode(1);
    AddNode(sapling, 1); //5
    conjured_sapling.addNode(1);
    AddNode(sapling, 1); //6
    conjured_sapling.addNode(1);
    AddNode(sapling, 2); //7
    conjured_sapling.addNode(2);
    AddNode(sapling, 2); //8
    conjured_sapling.addNode(2);
    AddNode(sapling, 2); //9
    conjured_sapling.addNode(2);
    AddNode(sapling, 3); //10
    conjured_sapling.addNode(3);
    AddNode(sapling, 3); //11
    conjured_sapling.addNode(3);
    AddNode(sapling, 3); //12
    conjured_sapling.addNode(3);

    AddNode(sapling, 8); //13
    conjured_sapling.addNode(8);
    AddNode(sapling, 8); //14
    conjured_sapling.addNode(8);
    AddNode(sapling, 8); //15
    conjured_sapling.addNode(8);
    ASSERT_TRUE(conjured_sapling.equals(sapling));

    DeleteNode(sapling, 8);
    conjured_sapling.deleteNode(8);
    ASSERT_TRUE(conjured_sapling.equals(sapling));

    DeleteSubtree(sapling, 2);
    conjured_sapling.deleteSubtree(2);
    ASSERT_TRUE(conjured_sapling.equals(sapling));
}

TEST(Tree, Split)
{
    Forest *fangorn = CreateForest(16);
    AddNode(fangorn, 0); //1
    AddNode(fangorn, 0); //2
    AddNode(fangorn, 0); //3
    AddNode(fangorn, 0); //4
    AddNode(fangorn, 0); //5
    AddNode(fangorn, 0); //6
    SplitNode(fangorn, 0, 3); //7

    ASSERT_EQ(RightmostChild(fangorn, 0), 7);
    ASSERT_EQ(RightmostChild(fangorn, 7), 6);
}

TEST(Tree, RandomNoSplit)
{
    const unsigned int op_count = 4096;
    const unsigned int check_interval = 256;
    const int seed = rand();
    RecordProperty("Seed", seed);
    std::mt19937 rng;
    rng.seed(seed);

    Forest *nordrasil = CreateForest(65536);
    SimpleTree conjured_nordrasil;

    for (unsigned int i = 0; i < op_count; ++i) {
        if (i % check_interval == 0)
            ASSERT_TRUE(conjured_nordrasil.equals(nordrasil)) << "Tree desynced after " << i << " operations";
        uint8_t op = (uint8_t)rng() % 3;
        uint32_t k;
        switch (op) {
            case 0: //ADD_NODE
                k = (uint32_t)(rng() % conjured_nordrasil.countNodes());
                if (conjured_nordrasil.hasNode(k))
                    continue;
                AddNode(nordrasil, k);
                conjured_nordrasil.addNode(k);
                break;
            case 1:
                if (conjured_nordrasil.countNodes() == 1)
                    continue;
                k = (uint32_t)(rng() % (conjured_nordrasil.countNodes() - 1) + 1);
                DeleteNode(nordrasil, k);
                conjured_nordrasil.deleteNode(k);
                break;
            case 2:
                if (conjured_nordrasil.countNodes() == 1)
                    continue;
                k = (uint32_t)(rng() % (conjured_nordrasil.countNodes() - 1) + 1);
                DeleteSubtree(nordrasil, k);
                conjured_nordrasil.deleteSubtree(k);
                break;
            default:
                ASSERT_TRUE(false) << "Invalid operation";
                break;
        }
    }
}


//************************************
// * ** *** POOR LITTLE MAIN  *** ** *
//************************************

int main(int argc, char **argv)
{
    std::srand((unsigned int)std::time(nullptr));
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

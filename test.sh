#!/bin/bash
#Składnia: ./test <Argumenty programu; w szczególności nic lub -v> <polecenie; w szczególności ./program> <katalog>
#Zmienne konfiguracyjne środowiska:
#  EXIT_ON_FAILED -- jeśli jest 1 to skritt przerwie uruchamianie po nieudanym teście (SHINIES!)
#  FAIL_KEEP_OUT -- jeśli jest 1 to po nieudanym teście outy nie będą usuwane
#  MUTE_DIFF

#RunOneTest <program> <test_name> <args>
function RunOneTest
{
    program=${1}
    shift
    test_core_name=${1}
    shift
    
    test_in=${test_core_name}.in
    test_out=${test_core_name}.out
    test_err=${test_core_name}.err
    stdout=$(mktemp)
    stderr=$(mktemp)
    fail="0"
    
    echo -e "\n\e[1;33mRunning test [${test_core_name}]...\e[0;0m"
    echo -e "Command: ${program} "$@" <${test_in} >${stdout} 2>${stderr}"
    if ${program} "$@" <${test_in} >${stdout} 2>${stderr}; then
        echo "Checking stdout (expected<  >produced):"
        if [ "$MUTE_DIFF" = 1 ]; then
            diff ${test_out} ${stdout} 2>/dev/null >&2
        else
            diff ${test_out} ${stdout} | sed "s/^/   /"
        fi
        
        if [ ${PIPESTATUS[0]} -eq 0 ]; then
            echo "Checking stderr (expected<  >produced):"
            if [ "$MUTE_DIFF" = 1 ]; then
                diff ${test_err} ${stderr} 2>/dev/null >&2
            else
                diff ${test_err} ${stderr} | sed "s/^/   /"
            fi
            
            if ! [ ${PIPESTATUS[0]} -eq 0 ]; then
                fail="1"
            fi
        else
            fail="1"
        fi
    else
        echo "Program exited with error code: ${?}"
        fail="1"
    fi
    
    if [ $fail = 0 ] || ! [ "$FAIL_KEEP_OUT" = 1 ]; then
        rm ${stdout} ${stderr}
    else
        echo Output not removed
    fi
    if [ $fail = 0 ]; then
        echo -e "\e[0;32mTest [${test_core_name}] passed\e[0;0m"
    else
        echo -e "\e[0;31mTest [${test_core_name}] failed\e[0;0m"
        if [ "$EXIT_ON_FAILED" = 1 ]; then
            exit
        fi
    fi
}


args=${@:1:$(($#-2))}
program=${@:$(($#-1)):1}
directory=${@:$#:1}
echo -e "\e[1;33m        Directory:\e[0;0m ${directory}"
echo -e "\e[1;33m          Program:\e[0;0m ${program}"
echo -e "\e[1;33mProgram arguments:\e[0;0m ${directory}"
for test_file in ${directory}/*.in; do
    RunOneTest ${program} ${test_file:0:(-3)} ${args}
done


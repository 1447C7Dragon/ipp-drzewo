#include <stdlib.h>
#include <memory.h>
#include <assert.h>
#include "tree.h"

/**
 * Get from forest node with given ID. Also assert that forest is not null and node with this id is not null
 * (btw also THE GAME). In fact the only reason for this function was to not repeat all these assertions
 * at the beginning of every function.
 * @param forest pointer to Forest (not null)
 * @param node id of node
 * @return Pointer to node from forest with given id
 */
static inline TreeNode *GetNode(Forest *forest, NodeIdT node)
{
    assert(forest != NULL);
    assert(node < forest->treeCounter);
    assert(forest->forest[node] != NULL);
    return forest->forest[node];
}

Forest *CreateForest(NodeIdT max_node_id)
{
    Forest *forester = (Forest*)malloc(sizeof(Forest));
    forester->treeCounter = 1;
    forester->size = 1;
    forester->forest = (TreeNode**)malloc(sizeof(TreeNode*) * max_node_id);
    memset(forester->forest, 0, sizeof(TreeNode*) * max_node_id);

    TreeNode *root = (TreeNode*)malloc(sizeof(TreeNode));
    root->children.head = NULL;
    root->children.tail = NULL;
    ListInsertAfter(&(root->children), NULL, 0u); //Dummy (see comment in AddNode)

    forester->forest[0] = root;
    return forester;
}

NodeIdT AddNode(Forest *forest, NodeIdT parent_id)
{
    TreeNode *parent = GetNode(forest, parent_id);
    if (parent->children.head == NULL) {
        //Insert Dummy to parent 's list
        ListInsertAfter(&(parent->children), NULL, 0u);
        /* We need only one dummy at the beginning of the list. It is needed when merging two lists in DeleteNode
         * to always keep head as it were before (to make list operations accept null list parameter) */
    }

    NodeIdT new_node_id = forest->treeCounter;
    TreeNode *new_node = (TreeNode*)malloc(sizeof(TreeNode));

    new_node->dock = ListInsertAfter(&(parent->children), parent->children.tail, new_node_id);
    new_node->children.head = NULL;
    new_node->children.tail = NULL;

    forest->forest[new_node_id] = new_node;
    forest->treeCounter += 1;
    forest->size += 1;

    return new_node_id;
}

NodeIdT RightmostChild(Forest *forest, NodeIdT node_id)
{
    TreeNode *node = GetNode(forest, node_id);

    /* It is possible that the list of children has some nodes at end that were deleted. Thus we need to iterate over
     * the list fo find last not NULL child and delete all NULLs in order to not do this again (for these nodes).
     * As we do exactly one iteration for each deleted node, the number of additional iterations do not exceeds number
     * of deleted nodes, so the time is amortized constant. */
    for (ListElement *child_element = node->children.tail; child_element != NULL;) {
        ListElement *prev = child_element->prev;
        if (forest->forest[child_element->data] == NULL && child_element->data != 0)
            ListErase(&(node->children), child_element);
        else
            return child_element->data;
        child_element = prev;
    }

    return 0u;
}

void DeleteNode(Forest *forest, NodeIdT node_id)
{
    assert(node_id != 0); //TODO we can free dock if is not tail
    if (node_id >= forest->treeCounter || forest->forest[node_id] == NULL)
        return;
    TreeNode *node_ptr = forest->forest[node_id];

    /* Insert children list before the dock. We insert before dock element and Dummy is always first, so we can omit
     * the first argument. */
    if (node_ptr->children.head != NULL)
        ListErase(&node_ptr->children, node_ptr->children.head); //Delete Dummy
    ListDevourAfter(NULL, node_ptr->dock->prev, &(node_ptr->children));
    forest->forest[node_id] = NULL;
    forest->size -= 1;
    free(node_ptr);
}

void DeleteSubtree(Forest *forest, NodeIdT node_id)
{
    assert(node_id != 0);
    if (node_id >= forest->treeCounter || forest->forest[node_id] == NULL)
        return;
    TreeNode *node_ptr = forest->forest[node_id];

    for (ListElement *child_element = node_ptr->children.head; child_element != NULL; child_element = child_element->next) {
        if (child_element->data != 0)
            DeleteSubtree(forest, child_element->data);
    }

    ListClear(&(node_ptr->children));
    free(node_ptr);
    forest->forest[node_id] = NULL;
    forest->size -= 1;

    /* We do not delete node's dock from parent's children list. It will be done when entire tree is deleted or in
     * RightmostChild function, but only if this node is in a suffix of list that contains only NULL children.
     * It is done this way in order to save memory (see comment in DeleteNode function). */
}

void SplitNode(Forest *forest, NodeIdT parent_node_id, NodeIdT pivot_node_id)
{
    TreeNode *parent = GetNode(forest, parent_node_id);
    TreeNode *pivot = GetNode(forest, pivot_node_id);

    List *new_node_children = ListSplitAfter(&(parent->children), pivot->dock);
    //Now we are after list split so the new node can be inserted like this
    NodeIdT new_node_id = AddNode(forest, parent_node_id);
    TreeNode *new_node = GetNode(forest, new_node_id);

    new_node->children.head = new_node_children->head;
    new_node->children.tail = new_node_children->tail;
    ListInsertAfter(&new_node->children, NULL, 0); //Dummy
    free(new_node_children);
}

void DeleteForest(Forest *forest)
{
    TreeNode *root = GetNode(forest, 0);

    for (ListElement *son = root->children.head; son != NULL; son = son->next) { //FIXME noldor?
        if (son->data != 0)
            DeleteSubtree(forest, son->data);
    }

    ListClear(&(root->children));
    free(root);
    free(forest);
}

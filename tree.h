/** @file
 * Implementation of the tree structure
 */
#ifndef ATREE_TREE_H
#define ATREE_TREE_H

#include <inttypes.h>

#include "list.h"

///TYpe used as nodes' ids
typedef uint32_t NodeIdT;

/**
 * Structure that stores information about single node in list
 */
typedef struct TreeNode
{
    ///List of node's children. First element is dummy and has unusable data.
    List children;

    ///Pointer to ListElement in parent's list where this node is docked
    ListElement *dock;
} TreeNode;

/**
 * Structure that stores information about entire tree.
 */
typedef struct Forest
{
    ///Counts tree nodes created so far
    NodeIdT treeCounter;

    ///Array mapping nodes' IDs to pointers to the nodes
    TreeNode **forest;

    ///Number of stored nodes
    NodeIdT size;
} Forest;

/**
 * Initialise entire structure. Allocates memory linear to number of max_node_id.
 * @param max_node_id maximal id of node that can be present in tree (in other words: number of nodes that are going to
 * be created over entire life of this tree.
 * @return pointer to initialised structure
 */
Forest *CreateForest(NodeIdT max_node_id);

/**
 * Delete all that remains from forest.
 * @param forest forest to be deleted
 */
void DeleteForest(Forest *forest);

/**
 * Create new node in forest whih given parent.
 * @param structure, to which node should be added
 * @param parent_id id of parent for new node
 * @return id of newly created node
 */
NodeIdT AddNode(Forest *forest, NodeIdT parent_id);

/**
 * Get rightmost child of given node (child that was not deleted). Return 0 if not present.
 * @note this works in amortized constant time. The amount of additional time spent here is limited to linear by number
 * of deleted nodes, that did not were handled in other call
 * @warning note that value returned when node has no children is 0, which looks like a valid id.
 * @param forest tre tree structure data
 * @param node_id id of examined node
 * @return id of rightmost child of given node or 0 if it has no children
 */
NodeIdT RightmostChild(Forest *forest, NodeIdT node_id);

/**
 * Remove single node from tree and put its children to parent in the same position the element was
 * (also preserve order).
 * @note This will not delete ListElement assigned with this node in its parent list. It will possibly be deleted in
 * next <c>RightmostChild</c> call (but it is not sure to happen).
 * @param forest tree structure
 * @param node_id id of node that will be deleted
 */
void DeleteNode(Forest *forest, NodeIdT node_id);

/**
 * Delete entire subtree.
 * @note The dock of subtree's root will not be deleted (se note in <c>DeleteNode</c>).
 * @param forest tree structure
 * @param node_id root of subtree that will be deleted
 */
void DeleteSubtree(Forest *forest, NodeIdT node_id);

/**
 * Insert new node after pivot node. All children that are on right side of new node will become its children.
 * @param forest tree structure
 * @param parent_node_id id of parent of pivot node
 * @param pivot_node_id  id of pivot node
 */
void SplitNode(Forest *forest, NodeIdT parent_node_id, NodeIdT pivot_node_id);

#endif //ATREE_TREE_H
